<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'name' => 'Voucher',
    'homeUrl' => '/admin',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'voucher' => [
            'class' => 'backend\modules\voucher\Voucher',
            'defaultRoute' => 'cadastro',
        ],
        'audit' => [
            'class'=>'bedezign\yii2\audit\Audit',
            'layout' => 'main',
            'db'=>'portal',
            'accessUsers' => [1, 3],
            'userIdentifierCallback' => ['app\models\User'],
            'trackActions' => ['voucher/validar/estornar','voucher/validar/utilizar'],
            'ignoreActions' => ['voucher/validar/emitido'],
            'panels' => [
                'audit/trail',
                'audit/request'
            ],
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
        ],
        'request' => [
            'baseUrl' => '/admin',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'view' => [
//            'theme' => [
//                'pathMap' => [
//                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
//                ],
//            ],
        ],
    ],
    'params' => $params,
];
