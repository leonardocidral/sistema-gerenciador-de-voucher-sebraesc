<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\SqlDataProvider;
use yii\widgets\Pjax;
use yii\grid\GridView;
use Mask\MaskFactory;
use Mask\MaskTypes;

/* @var $this yii\web\View */
/* @var $model backend\modules\voucher\models\Cadastro */

$this->title = $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Cadastros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Informações pessoais</a></li>
        <li><a href="#tab_2" data-toggle="tab">Vouchers</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'nome',
                    'login',
                    [                      // the owner name of the model
                        'label' => 'CPF',
                        'value' => MaskFactory::factory(MaskTypes::MASK_CPF, str_pad( str_replace( ['.','-','/',''], '', $model->cpf),11,0,STR_PAD_LEFT))->getMasked(),
                    ],
                    'nascimento:date',
                    'sexo',
                    [
                        'label' => 'Celular',
                        'value' => MaskFactory::factory(MaskTypes::MASK_TELEFONE, $model->telefone_celular)->getMasked(),
                    ],
                    [
                        'label' => 'Telefone residencial',
                        'value' => MaskFactory::factory(MaskTypes::MASK_TELEFONE, $model->telefone_residencial)->getMasked(),
                    ],
                    'email:email',
                    [
                        'label' => 'Data do cadastro',
                        'format' => 'datetime',
                        'value' => $model->created
                    ]
                ],
            ]) ?>
        </div>
        <div class="tab-pane" id="tab_2">
            <?php
            $sql = "
                SELECT
                    c.nome,
                    ca.pk_cadastro_aplicacao
                FROM
                    cadastro c
                    inner join cadastro_aplicacao ca on ca.cadastro_id=c.id
                    inner join aplicacao a on a.id=2 and ca.aplicacao_id=a.id
                WHERE
                    c.id = {$model->id}
            ";
            $model = Yii::$app->central->createCommand($sql);
            $users_id = $model->queryOne()["pk_cadastro_aplicacao"];


            $totalCount = Yii::$app->portal->createCommand('SELECT COUNT(*) FROM voucher WHERE user_id=:userID', [':userID' => $users_id])->queryScalar();
            $dataProviderVoucher = new SqlDataProvider([
                'sql' => 'SELECT * FROM voucher WHERE user_id=:userID ORDER BY expira_em DESC',
                'db' => Yii::$app->portal,
                'params' => [':userID' => $users_id],
                'totalCount' => $totalCount,
                'sort' =>false, //to remove the table header sorting
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            ?>

            <div class="box-body table-responsive">
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProviderVoucher,
                    'columns' => [
                        'recompensa',
                        [
                            'attribute' => 'Número',
                            'format' => 'raw',
                            'value' => function ($model) {
                                switch ( $model["status"] ) {
                                    case "emitido":
                                        return Html::a($model["uuid"], ['validar/emitido', 'id' => $model["id"]], ['style'=>'font-weight:bolder;']);
                                        break;
                                    case "expirado":
                                        return $model["uuid"];
                                        break;
                                    case "estornado":
                                        return $model["uuid"];
                                        break;
                                    case "utilizado":
                                        return $model["uuid"];
                                        break;
                                }
                            }
                        ],
                        'criado_em:datetime',
                        'expira_em:datetime',
                        [
                            'attribute' => 'Status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                switch ( $model["status"] ) {
                                    case "emitido":
                                        return "<span class=\"label label-primary\">{$model["status"]}</span>";
                                        break;
                                    case "expirado":
                                        return "<span class=\"label label-danger\">{$model["status"]}</span>";
                                        break;
                                    case "estornado":
                                        return "<span class=\"label label-warning\">{$model["status"]}</span>";
                                        break;
                                    case "utilizado":
                                        return "<span class=\"label label-success\">{$model["status"]}</span>";
                                        break;
                                }
                            },
                        ],
                        'atualizado_em:datetime'
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>


        </div>
    </div>
    <!-- /.tab-content -->
</div>
