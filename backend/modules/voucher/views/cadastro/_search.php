<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\voucher\models\CadastroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .help-block {
        display:none;
    }
</style>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        ],
        ['fieldConfig' => ['template' => '{input}']]);
    ?>

<div class="row">
    <div class="input-group-btn">
        <div class="col-xs-4">
            <?php echo $form->field($model, 'cnpj')->label(false)->textInput(['maxlength' => 36, 'style'=>'width:200px;', 'class' => 'form-control pull-right', 'placeholder' => 'CNPJ']) ?>
        </div>
        <div class="col-xs-4">
            <?php echo $form->field($model, 'nome')->label(false)->textInput(['maxlength' => 36, 'style'=>'width:200px;', 'class' => 'form-control pull-right', 'placeholder' => 'Nome do usuário']) ?>
        </div>
        <div class="col-xs-4">
            <?= Html::submitButton('<i class="fa fa-search"></i>', ['style'=>'float:right;','class' => 'btn btn-default']) ?>
            <?php echo $form->field($model, 'cpf')->textInput(['maxlength' => 14, 'style'=>'width:200px;', 'class' => 'form-control pull-right', 'placeholder' => 'CPF'])->label(false) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
