<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\SqlDataProvider;
use Mask\MaskFactory;
use Mask\MaskTypes;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\voucher\models\CadastroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cadastros';
$this->params['breadcrumbs'][] = $this->title;

$sql = "
select
  c.id,
  c.nome,
  c.nascimento,
  c.cpf,
  c.sexo,
  (
	SELECT
		e.cnpj
	FROM
		cadastro_empresa ce
		INNER JOIN empresa e ON e.id = ce.empresa_id
	WHERE
		ce.cadastro_id=c.id
  ) as cnpj
from
  cadastro c
";

$sqlCount = "
select
  count(*)
from
  cadastro c
";

if (isset($_GET['CadastroSearch'])) {
    $filtro = [];
    if (isset($_GET['CadastroSearch']['cpf']) && $_GET['CadastroSearch']['cpf'] != "") {
        $cpf = (int) str_replace( ['.','-','/',''], '',  $_GET['CadastroSearch']['cpf']);
        $filtro[] = "cpf like '%{$cpf}%'";
    }
    if (isset($_GET['CadastroSearch']['nome']) && $_GET['CadastroSearch']['nome'] != "") {
        $filtro[] = "nome like '%{$_GET['CadastroSearch']['nome']}%'";
    }
    if (isset($_GET['CadastroSearch']['cnpj']) && $_GET['CadastroSearch']['cnpj'] != "") {
        $cnpj = (int) str_replace( ['.','-','/',''], '',  $_GET['CadastroSearch']['cnpj']);
        $filtro[] = "(SELECT e.cnpj FROM cadastro_empresa ce INNER JOIN empresa e ON e.id = ce.empresa_id WHERE ce.cadastro_id=c.id) like '%{$cnpj}%'";
    }
    if (count($filtro) > 0) {
        $sql .= " WHERE ".join(' AND ',$filtro);
        $sqlCount .= " WHERE ".join(' AND ',$filtro);
    }
}

$totalCount = Yii::$app->central->createCommand($sqlCount)->queryScalar();
$dataProvider = new SqlDataProvider([
    'sql' => $sql,
    'db' => Yii::$app->central,
    'totalCount' => $totalCount,
    'sort' =>false, //to remove the table header sorting
    'pagination' => [
        'pageSize' => 10,
    ],
]);
?>

<!--div class="cadastro-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
</div-->

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Filtro</h3>
        <div class="box-tools">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>


    <!-- /.box-header -->
    <div class="box-body table-responsive">
        <?php Pjax::begin(['id' => 'cadastro']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'Nome',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model['nome'], ['view', 'id' => $model['id']]);
                    },
                ],
                [
                    'attribute' => 'Cpf',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $cpf = str_replace( ['-','.','/',''], '', $model['cpf']);
                        $cpf = str_pad( $cpf, 11, 0, STR_PAD_LEFT);
                        return MaskFactory::factory(MaskTypes::MASK_CPF, $cpf)->getMasked();
                    },
                ],
                [
                    'attribute' => 'CNPJ',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if (is_null($model['cnpj']) || empty($model['cnpj'])) return '--';

                        $cnpj = str_replace( ['-','.','/',''], '', $model['cnpj']);
                        $cnpj = str_pad( $cnpj, 14, 0, STR_PAD_LEFT);

                        return MaskFactory::factory(MaskTypes::MASK_CNPJ, str_replace( ['-','.','/',''], '', $cnpj))->getMasked();
                    },
                ],
                'nascimento:date',
                'sexo'
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <!-- /.box-body -->
</div>
