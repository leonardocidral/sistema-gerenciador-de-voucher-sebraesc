<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\voucher\models\VoucherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vouchers';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">
    <div class="box-header">
        <h3 class="box-title">Filtro</h3>
        <div class="box-tools">
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    <div class="box-body table-responsive">

        <?= Yii::$app->session->hasFlash("voucher") ? \insolita\wgadminlte\Alert::widget(Yii::$app->session->getFlash("voucher")) : "";?>

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'Nome',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->wpUsers->display_name;
//                            return Html::a($model->wpUsers->display_name, ['cadastro/view', 'id' => $model->wpUsers->ID], ['style'=>'font-weight:bolder;']);
                        }
                    ],
                    [
                        'attribute' => 'Número',
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ( $model["status"] ) {
                                case "emitido":
                                    return Html::a($model["uuid"], ['emitido', 'id' => $model["id"]], ['style'=>'font-weight:bolder;']);
                                    break;
                                case "expirado":
                                    return $model["uuid"];
                                    break;
                                case "estornado":
                                    return $model["uuid"];
                                    break;
                                case "utilizado":
                                    return $model["uuid"];
                                    break;
                            }
                        }
                    ],
                    'criado_em:datetime',
                    'expira_em:datetime',
                    [
                        'attribute' => 'Status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            switch ( $model["status"] ) {
                                case "emitido":
                                    return "<span class=\"label label-primary\">{$model["status"]}</span>";
                                    break;
                                case "expirado":
                                    return "<span class=\"label label-danger\">{$model["status"]}</span>";
                                    break;
                                case "estornado":
                                    return "<span class=\"label label-warning\">{$model["status"]}</span>";
                                    break;
                                case "utilizado":
                                    return "<span class=\"label label-success\">{$model["status"]}</span>";
                                    break;
                            }
                        },
                    ],
                    'atualizado_em:datetime'
                ],
            ]); ?>
        <?php Pjax::end(); ?>
        </div>
    </div>
</div>