<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\voucher\models\Voucher */

$this->title = 'Voucher';
$this->params['breadcrumbs'][] = ['label' => 'Vouchers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];

$class = 'box-primary';
if ($model->status == 'emitido') {
    $class = 'box-primary';
}

if ($model->status == 'utilizado') {
    $class = 'box-success';
}
\bedezign\yii2\audit\web\JSLoggingAsset::register($this);
?>

<div class="box <?= $class; ?>">
    <div class="box-body table-responsive">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>