<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\voucher\models\Voucher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="voucher-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= DetailView::widget( [
		'model'      => $model,
		'attributes' => [
			'uuid',
			'recompensa',
			'criado_em:datetime',
			'expira_em:datetime',
			'status'
		],
	] ) ?>

	<div class="form-group">

		<?php if ($model["status"] == 'emitido') { ?>

		<?= Html::a( "Utilizar", ['utilizar', 'id' => $model["id"]], [ 'class' => 'btn btn-lg btn-success' ] ) ?>
		&nbsp;
		<?= Html::a( 'Estornar', ['estornar', 'id' => $model["id"]], [ 'class' => 'btn btn-lg btn-danger' ] ) ?>
		&nbsp;
		<?php } ?>
		<?= Html::a( 'Voltar', Yii::$app->request->referrer, [ 'class' => 'btn btn-lg btn-default' ] ) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
