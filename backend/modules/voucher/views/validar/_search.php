<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\voucher\models\CadastroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .help-block {
        display:none;
    }
</style>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
],
    ['fieldConfig' => ['template' => '{input}']]);
?>

<div class="row">
    <div class="input-group-btn">
        <div class="col-xs-3">
            <?php //echo $form->field($model, 'cpf')->textInput(['maxlength' => 11, 'style'=>'width:100px;', 'class' => 'form-control pull-right', 'placeholder' => 'CPF'])->label(false) ?>
        </div>
        <div class="col-xs-4">
            <?php echo $form->field($model, 'status')->label(false)->dropDownList([ 'emitido' => 'Emitido', 'expirado' => 'Expirado', 'estornado' => 'Estornado', 'utilizado' => 'Utilizado', ], ['prompt' => 'Selecione um status', 'style'=>'width:200px;','onchange'=>'this.form.submit()']) ?>
        </div>
        <div class="col-xs-5">
            <?= Html::submitButton('<i class="fa fa-search"></i>', ['style'=>'float:right;','class' => 'btn btn-default']) ?>
            <?php echo $form->field($model, 'uuid')->textInput(['maxlength' => 36, 'style'=>'width:300px;', 'class' => 'form-control pull-right', 'placeholder' => 'Número do voucher'])->label(false) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
