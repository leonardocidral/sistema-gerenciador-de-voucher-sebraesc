<?php

namespace backend\modules\voucher\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\voucher\models\Cadastro;

/**
 * CadastroSearch represents the model behind the search form about `backend\modules\voucher\models\Cadastro`.
 */
class CadastroSearch extends Cadastro
{

    public $cnpj;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'endereco_id', 'sincroniaCompleta', 'preCadastro', 'modifier_id', 'creator_id'], 'integer'],
            [['nome', 'login', 'nascimento', 'sexo', 'telefone_celular', 'telefone_residencial', 'email', 'numero', 'complemento', 'cep', 'created', 'modified', 'cpf', 'cnpj'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cadastro::find();

        // add conditions that should always apply here
        $query->where('id > 1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'endereco_id' => $this->endereco_id,
            'sincroniaCompleta' => $this->sincroniaCompleta,
            'preCadastro' => $this->preCadastro,
            'cpf' => str_replace( ['.','-',' '], '', $this->cpf),
            'nascimento' => $this->nascimento,
            'created' => $this->created,
            'modified' => $this->modified,
            'modifier_id' => $this->modifier_id,
            'creator_id' => $this->creator_id,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'sexo', $this->sexo])
            ->andFilterWhere(['like', 'telefone_celular', $this->telefone_celular])
            ->andFilterWhere(['like', 'telefone_residencial', $this->telefone_residencial])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'numero', $this->numero])
            ->andFilterWhere(['like', 'complemento', $this->complemento])
            ->andFilterWhere(['like', 'cep', $this->cep]);

        return $dataProvider;
    }
}
