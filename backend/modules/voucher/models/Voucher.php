<?php

namespace backend\modules\voucher\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "voucher".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $recompensa
 * @property string $uuid
 * @property string $status
 * @property string $expira_em
 * @property string $criado_em
 * @property string $atualizado_em
 */
class Voucher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voucher';
    }

    public static function getDb() {
        return Yii::$app->portal;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'recompensa', 'uuid', 'expira_em', 'criado_em'], 'required'],
            [['user_id'], 'integer'],
            [['status'], 'string'],
            [['expira_em', 'criado_em', 'atualizado_em'], 'safe'],
            [['recompensa'], 'string', 'max' => 255],
            [['uuid'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => wpUsers::className(), 'targetAttribute' => ['user_id' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'recompensa' => 'Recompensa',
            'uuid' => 'Número',
            'status' => 'Status',
            'expira_em' => 'Expira Em',
            'criado_em' => 'Criado Em',
            'atualizado_em' => 'Atualizado Em',
            'returnMessage' => 'Mensagem de retorno'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWpUsers()
    {
        return $this->hasOne(wpUsers::className(), ['ID' => 'user_id']);
    }

    public static function atualizaExpirados() {
        Yii::$app->portal->createCommand("UPDATE voucher SET status='expirado', atualizado_em=NOW() WHERE status='emitido' and expira_em < NOW()")->execute();
    }

    public function getCpf() {
        $sql = "
                SELECT
                    c.cpf
                FROM
                    cadastro c
                    inner join cadastro_aplicacao ca on ca.cadastro_id=c.id
                    inner join aplicacao a on a.id=2 and ca.aplicacao_id=a.id
                WHERE
                    ca.pk_cadastro_aplicacao = {$this->user_id}
            ";
        $rs = Yii::$app->central->createCommand($sql)->queryOne();

        return str_pad($rs["cpf"], 11, "0", STR_PAD_LEFT);
    }

    public function utilizar() {
        $this->atualizado_em = new Expression('NOW()');
        $this->status = 'utilizado';
        Yii::$app->session->setFlash('voucher',[
            'type' => 'success',
            'icon' => 'icon fa fa-check',
            'text' => '&nbsp;&nbsp;Voucher atualizado com sucesso!'
        ]);
        return $this->save();
    }

    public function estornar() {
        try {
            $gamification = json_decode(getenv('GAMIFICATION'));
            $url = "{$gamification->host}/rest/userRewardRefoundByName/{$gamification->passwd}/PORTAL_ATENDIMENTO/{$this->getCpf()}/{$this->recompensa}.json";
            $client = new \GuzzleHttp\Client([
                'Content-Type' => 'application/json',
            ]);
            $response = $client->request('GET', $url);
            $result = \GuzzleHttp\json_decode($response->getBody()->getContents());
            if ($result->dynamo->returnCode == 1) {
                $this->status = "estornado";
                $this->atualizado_em = new Expression('NOW()');
                if ($this->save()) {
                    Yii::$app->session->setFlash('voucher',[
                        'type' => 'warning',
                        'icon' => 'icon fa fa-check',
                        'text' => '&nbsp;&nbsp;Voucher estornado com sucesso!' //$result->dynamo->returnMessage
                    ]);
                } else {
                    Yii::$app->session->setFlash('voucher',[
                        'type' => 'error',
                        'icon' => 'icon fa fa-ban',
                        'text' => '&nbsp;&nbsp;'.$this->errors
                    ]);
                }
                return true;
            }
            Yii::$app->session->setFlash('voucher',[
                'type' => 'error',
                'icon' => 'icon fa fa-ban',
                'text' => '&nbsp;&nbsp;'.$result->dynamo->returnMessage."<br>".$result->dynamo->returnErrors
            ]);
            Yii::error(['dynamo'=>$result->dynamo->returnMessage, 'urlDynamo'=>$url],'dynamo');
        } catch (\RequestExeception $e) {
            Yii::error($e->getResponse()->getBody()->getContents(),'voucher');
        }
        return false;
    }

    public function behaviors()
    {
        return [
            'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }

}
