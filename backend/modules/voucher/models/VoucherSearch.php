<?php

namespace backend\modules\voucher\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\voucher\models\Voucher;

/**
 * VoucherSearch represents the model behind the search form about `backend\modules\voucher\models\Voucher`.
 */
class VoucherSearch extends Voucher
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['recompensa', 'uuid', 'status', 'expira_em', 'criado_em'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Voucher::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'expira_em' => $this->expira_em,
            'criado_em' => $this->criado_em,
        ]);

        $query->andFilterWhere(['like', 'recompensa', $this->recompensa])
            ->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'status', $this->status]);

        $query->orderBy(['criado_em'=>SORT_DESC]);

        return $dataProvider;
    }
}
