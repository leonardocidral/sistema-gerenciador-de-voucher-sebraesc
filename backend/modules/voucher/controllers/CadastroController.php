<?php

namespace backend\modules\voucher\controllers;

use backend\modules\voucher\models\Voucher;
use Yii;
use backend\modules\voucher\models\Cadastro;
use backend\modules\voucher\models\CadastroSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CadastroController implements the CRUD actions for Cadastro model.
 */
class CadastroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * Lists all Cadastro models.
     * @return mixed
     */
    public function actionIndex()
    {
        Voucher::atualizaExpirados();

        $searchModel = new CadastroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionVoucher()
    {
        return $this->render('voucher', [
            'uuid' => Yii::$app->request->get('uuid')
        ]);
    }


    /**
     * Displays a single Cadastro model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Finds the Cadastro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cadastro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cadastro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
