<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "empresa_aplicacao".
 *
 * @property integer $id
 * @property integer $empresa_id
 * @property integer $aplicacao_id
 */
class EmpresaAplicacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empresa_aplicacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empresa_id', 'aplicacao_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'aplicacao_id' => 'Aplicacao ID',
        ];
    }
}
