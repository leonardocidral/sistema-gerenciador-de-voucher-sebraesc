<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property integer $id
 * @property integer $endereco_id
 * @property string $nome_fantasia
 * @property string $razao_social
 * @property string $cnpj
 * @property string $cpfPrincipal
 * @property string $dtFundacao
 * @property string $porte
 * @property integer $qtFuncionarios
 * @property string $telefone
 * @property integer $setor
 *
 * @property CadastroEmpresa[] $cadastroEmpresas
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['endereco_id', 'cnpj', 'cpfPrincipal', 'qtFuncionarios', 'setor'], 'integer'],
            [['cnpj'], 'required'],
            [['porte'], 'string'],
            [['nome_fantasia', 'razao_social'], 'string', 'max' => 120],
            [['dtFundacao'], 'string', 'max' => 8],
            [['telefone'], 'string', 'max' => 20],
            [['cnpj'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'endereco_id' => 'Endereco ID',
            'nome_fantasia' => 'Nome Fantasia',
            'razao_social' => 'Razao Social',
            'cnpj' => 'Cnpj',
            'cpfPrincipal' => 'Cpf Principal',
            'dtFundacao' => 'Dt Fundacao',
            'porte' => 'Porte',
            'qtFuncionarios' => 'Qt Funcionarios',
            'telefone' => 'Telefone',
            'setor' => 'Setor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastroEmpresas()
    {
        return $this->hasMany(CadastroEmpresa::className(), ['empresa_id' => 'id']);
    }
}
