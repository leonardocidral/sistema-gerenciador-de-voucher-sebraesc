<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "configuracao".
 *
 * @property string $configuracao_nome
 */
class Configuracao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configuracao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['configuracao_nome'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'configuracao_nome' => 'Configuracao Nome',
        ];
    }
}
