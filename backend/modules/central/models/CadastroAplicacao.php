<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "cadastro_aplicacao".
 *
 * @property integer $id
 * @property integer $aplicacao_id
 * @property integer $cadastro_id
 * @property integer $pk_cadastro_aplicacao
 * @property string $tipoOperacao
 * @property string $created
 * @property integer $creator_id
 *
 * @property Aplicacao $aplicacao
 * @property Cadastro $cadastro
 */
class CadastroAplicacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cadastro_aplicacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aplicacao_id', 'cadastro_id', 'pk_cadastro_aplicacao', 'created', 'creator_id'], 'required'],
            [['aplicacao_id', 'cadastro_id', 'pk_cadastro_aplicacao', 'creator_id'], 'integer'],
            [['tipoOperacao'], 'string'],
            [['created'], 'safe'],
            [['aplicacao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Aplicacao::className(), 'targetAttribute' => ['aplicacao_id' => 'id']],
            [['cadastro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cadastro::className(), 'targetAttribute' => ['cadastro_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'aplicacao_id' => 'Aplicacao ID',
            'cadastro_id' => 'Cadastro ID',
            'pk_cadastro_aplicacao' => 'Pk Cadastro Aplicacao',
            'tipoOperacao' => 'Tipo Operacao',
            'created' => 'Created',
            'creator_id' => 'Creator ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAplicacao()
    {
        return $this->hasOne(Aplicacao::className(), ['id' => 'aplicacao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastro()
    {
        return $this->hasOne(Cadastro::className(), ['id' => 'cadastro_id']);
    }
}
