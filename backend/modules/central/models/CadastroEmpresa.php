<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "cadastro_empresa".
 *
 * @property integer $id
 * @property integer $cadastro_id
 * @property integer $empresa_id
 * @property string $proprietario
 *
 * @property Cadastro $cadastro
 * @property Empresa $empresa
 */
class CadastroEmpresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cadastro_empresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cadastro_id', 'empresa_id'], 'required'],
            [['cadastro_id', 'empresa_id'], 'integer'],
            [['proprietario'], 'string'],
            [['cadastro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cadastro::className(), 'targetAttribute' => ['cadastro_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cadastro_id' => 'Cadastro ID',
            'empresa_id' => 'Empresa ID',
            'proprietario' => 'Proprietario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastro()
    {
        return $this->hasOne(Cadastro::className(), ['id' => 'cadastro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }
}
