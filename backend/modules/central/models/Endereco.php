<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "endereco".
 *
 * @property integer $id
 * @property integer $comercial
 * @property string $cep
 * @property string $complemento
 * @property string $numero
 * @property string $logradouro
 * @property string $bairro
 * @property string $uf
 * @property string $cidade
 *
 * @property Cadastro[] $cadastros
 */
class Endereco extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'endereco';
    }
    public static function getDb() {
        return Yii::$app->central;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comercial'], 'required'],
            [['comercial'], 'integer'],
            [['cep'], 'string', 'max' => 8],
            [['complemento'], 'string', 'max' => 120],
            [['numero'], 'string', 'max' => 10],
            [['logradouro'], 'string', 'max' => 255],
            [['bairro', 'cidade'], 'string', 'max' => 100],
            [['uf'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comercial' => 'Comercial',
            'cep' => 'Cep',
            'complemento' => 'Complemento',
            'numero' => 'Numero',
            'logradouro' => 'Logradouro',
            'bairro' => 'Bairro',
            'uf' => 'Uf',
            'cidade' => 'Cidade',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastros()
    {
        return $this->hasMany(Cadastro::className(), ['endereco_id' => 'id']);
    }
}
