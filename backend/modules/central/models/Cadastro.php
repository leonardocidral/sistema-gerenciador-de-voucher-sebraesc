<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "cadastro".
 *
 * @property integer $id
 * @property integer $endereco_id
 * @property integer $sincroniaCompleta
 * @property integer $preCadastro
 * @property string $nome
 * @property string $login
 * @property string $cpf
 * @property string $nascimento
 * @property string $sexo
 * @property string $telefone_celular
 * @property string $telefone_residencial
 * @property string $email
 * @property string $numero
 * @property string $complemento
 * @property string $cep
 * @property string $created
 * @property string $modified
 * @property integer $modifier_id
 * @property integer $creator_id
 *
 * @property Endereco $endereco
 * @property CadastroAplicacao[] $cadastroAplicacaos
 * @property CadastroEmpresa[] $cadastroEmpresas
 */
class Cadastro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cadastro';
    }

    public static function getDb() {
        return Yii::$app->central;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['endereco_id', 'sincroniaCompleta', 'preCadastro', 'cpf', 'modifier_id', 'creator_id'], 'integer'],
            [['cpf', 'created', 'modified', 'modifier_id', 'creator_id'], 'required'],
            [['nascimento', 'created', 'modified'], 'safe'],
            [['sexo'], 'string'],
            [['nome'], 'string', 'max' => 100],
            [['login'], 'string', 'max' => 20],
            [['telefone_celular'], 'string', 'max' => 14],
            [['telefone_residencial'], 'string', 'max' => 13],
            [['email'], 'string', 'max' => 255],
            [['numero'], 'string', 'max' => 10],
            [['complemento'], 'string', 'max' => 120],
            [['cep'], 'string', 'max' => 8],
            [['cpf'], 'unique'],
            [['login'], 'unique'],
            [['endereco_id'], 'exist', 'skipOnError' => true, 'targetClass' => Endereco::className(), 'targetAttribute' => ['endereco_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Usuário que fez o cadastro no sistema. ',
            'endereco_id' => 'Endereco ID',
            'sincroniaCompleta' => 'Sincronia Completa',
            'preCadastro' => 'Pre Cadastro',
            'nome' => 'Nome',
            'login' => 'Login',
            'cpf' => 'Cpf',
            'nascimento' => 'Nascimento',
            'sexo' => 'Sexo',
            'telefone_celular' => 'Telefone Celular',
            'telefone_residencial' => 'Telefone Residencial',
            'email' => 'Email',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'cep' => 'Cep',
            'created' => 'Created',
            'modified' => 'Modified',
            'modifier_id' => 'Modifier ID',
            'creator_id' => 'Creator ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndereco()
    {
        return $this->hasOne(Endereco::className(), ['id' => 'endereco_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastroAplicacaos()
    {
        return $this->hasMany(CadastroAplicacao::className(), ['cadastro_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastroEmpresas()
    {
        return $this->hasMany(CadastroEmpresa::className(), ['cadastro_id' => 'id']);
    }
}
