<?php

namespace backend\modules\central\models;

use Yii;

/**
 * This is the model class for table "aplicacao".
 *
 * @property integer $id
 * @property string $alias
 *
 * @property CadastroAplicacao[] $cadastroAplicacaos
 */
class Aplicacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aplicacao';
    }

    public static function getDb() {
        return Yii::$app->central;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Aplicações utilizadas pelo cliente.',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadastroAplicacaos()
    {
        return $this->hasMany(CadastroAplicacao::className(), ['aplicacao_id' => 'id']);
    }
}
