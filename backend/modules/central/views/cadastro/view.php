<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\central\models\Cadastro */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cadastros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cadastro-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'endereco_id',
            'sincroniaCompleta',
            'preCadastro',
            'nome',
            'login',
            'cpf',
            'nascimento',
            'sexo',
            'telefone_celular',
            'telefone_residencial',
            'email:email',
            'numero',
            'complemento',
            'cep',
            'created',
            'modified',
            'modifier_id',
            'creator_id',
        ],
    ]) ?>

</div>
