<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\central\models\CadastroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cadastro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'endereco_id') ?>

    <?= $form->field($model, 'sincroniaCompleta') ?>

    <?= $form->field($model, 'preCadastro') ?>

    <?= $form->field($model, 'nome') ?>

    <?php // echo $form->field($model, 'login') ?>

    <?php // echo $form->field($model, 'cpf') ?>

    <?php // echo $form->field($model, 'nascimento') ?>

    <?php // echo $form->field($model, 'sexo') ?>

    <?php // echo $form->field($model, 'telefone_celular') ?>

    <?php // echo $form->field($model, 'telefone_residencial') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'numero') ?>

    <?php // echo $form->field($model, 'complemento') ?>

    <?php // echo $form->field($model, 'cep') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'modified') ?>

    <?php // echo $form->field($model, 'modifier_id') ?>

    <?php // echo $form->field($model, 'creator_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
