<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\central\models\Cadastro */

$this->title = 'Update Cadastro: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cadastros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cadastro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
