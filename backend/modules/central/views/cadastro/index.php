<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\central\models\CadastroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cadastros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cadastro-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cadastro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>

        <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nome',
                'login',
                'cpf',
                'nascimento',
                'sexo',
                [
                    'attribute'=>'Cidade',
                    'value'=>'endereco.cidade',
//                'contentOptions'=>['style'=>'width: 120px;']
                ],
                // 'telefone_celular',
                // 'telefone_residencial',
                // 'email:email',
                // 'numero',
                // 'complemento',
                // 'cep',
                // 'created',
                // 'modified',
                // 'modifier_id',
                // 'creator_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <!-- /.box-body -->
</div>
