<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\central\models\Aplicacao */

$this->title = 'Create Aplicacao';
$this->params['breadcrumbs'][] = ['label' => 'Aplicacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aplicacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
