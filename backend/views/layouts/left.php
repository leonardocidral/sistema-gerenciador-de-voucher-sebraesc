<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/admin/img/sebrae.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo \Yii::$app->user->identity->username; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form --
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Cadastros', 'icon' => 'fa fa-users', 'url' => ['/voucher/cadastro'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                    ['label' => 'Voucher', 'icon' => 'fa fa-sticky-note', 'url' => ['/voucher/validar'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                    ['label' => 'Audit Trail', 'icon' => 'fa fa-file-code-o', 'url' => ['/audit'], 'visible' => Yii::$app->user->identity->username == 'leonardo.cidral'],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'], 'visible' => Yii::$app->user->identity->username == 'leonardo.cidral'],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'], 'visible' => Yii::$app->user->identity->username == 'leonardo.cidral'],
                ],
            ]
        ) ?>

    </section>

</aside>
