<?php
namespace console\controllers;

use common\models\User;
use Yii;
use yii\console\Controller;

class HelloController extends Controller {

	public function actionIndex($username,$password,$email) {

		$user = new User();
		$user->username = $username;
		$user->email = $email;
		$user->setPassword($password);
		$user->generateAuthKey();
		if ($user->save()) {
			echo "\nUsuário criado com sucesso!\n";
		} else {
			echo "\nErro ao criar um novo usuário!\n";
		}
	}

}